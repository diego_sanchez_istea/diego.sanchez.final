-- MySQL dump 10.16  Distrib 10.1.32-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: dhsbt
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DATABASECHANGELOG`
--

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `AUTHOR` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `FILENAME` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `MD5SUM` varchar(35) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `LIQUIBASE` varchar(20) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `CONTEXTS` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `LABELS` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOG`
--

LOCK TABLES `DATABASECHANGELOG` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOG` DISABLE KEYS */;
/*!40000 ALTER TABLE `DATABASECHANGELOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATABASECHANGELOGLOCK`
--

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOGLOCK`
--

LOCK TABLES `DATABASECHANGELOGLOCK` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOGLOCK` VALUES (1,'\0',NULL,NULL);
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_attachment`
--

DROP TABLE IF EXISTS `phpbt_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_attachment` (
  `attachment_id` int(10) unsigned NOT NULL DEFAULT '0',
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `file_name` char(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `bytes` longblob,
  `description` char(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `file_size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `mime_type` char(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`attachment_id`),
  KEY `bug_id_attachment` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_attachment`
--

LOCK TABLES `phpbt_attachment` WRITE;
/*!40000 ALTER TABLE `phpbt_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_attachment_seq`
--

DROP TABLE IF EXISTS `phpbt_attachment_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_attachment_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_attachment_seq`
--

LOCK TABLES `phpbt_attachment_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_attachment_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_attachment_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_auth_group`
--

DROP TABLE IF EXISTS `phpbt_auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_auth_group` (
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_name` varchar(80) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `is_role` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_auth_group`
--

LOCK TABLES `phpbt_auth_group` WRITE;
/*!40000 ALTER TABLE `phpbt_auth_group` DISABLE KEYS */;
INSERT INTO `phpbt_auth_group` VALUES (1,'Admin',1,0,0,0,0,0),(2,'User',1,0,0,0,0,0),(3,'Developer',0,0,0,0,0,0),(4,'Manager',0,0,0,0,0,0),(5,'Guest',1,0,0,0,0,1),(6,'User',1,0,0,0,0,1),(7,'Reporter',1,0,0,0,0,1),(8,'Assignee',1,0,0,0,0,1),(9,'Owner',1,0,0,0,0,1);
/*!40000 ALTER TABLE `phpbt_auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_auth_group_seq`
--

DROP TABLE IF EXISTS `phpbt_auth_group_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_auth_group_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_auth_group_seq`
--

LOCK TABLES `phpbt_auth_group_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_auth_group_seq` DISABLE KEYS */;
INSERT INTO `phpbt_auth_group_seq` VALUES (9);
/*!40000 ALTER TABLE `phpbt_auth_group_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_auth_perm`
--

DROP TABLE IF EXISTS `phpbt_auth_perm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_auth_perm` (
  `perm_id` int(10) unsigned NOT NULL DEFAULT '0',
  `perm_name` varchar(80) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`perm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_auth_perm`
--

LOCK TABLES `phpbt_auth_perm` WRITE;
/*!40000 ALTER TABLE `phpbt_auth_perm` DISABLE KEYS */;
INSERT INTO `phpbt_auth_perm` VALUES (1,'Admin',0,0,0,0),(2,'AddBug',0,0,0,0),(3,'EditAssignment',0,0,0,0),(4,'Assignable',0,0,0,0),(5,'EditBug',0,0,0,0),(6,'CloseBug',0,0,0,0),(7,'CommentBug',0,0,0,0),(8,'EditPriority',0,0,0,0),(9,'EditStatus',0,0,0,0),(10,'EditSeverity',0,0,0,0),(11,'EditResolution',0,0,0,0),(12,'EditProject',0,0,0,0),(13,'EditComponent',0,0,0,0),(14,'ManageBug',0,0,0,0);
/*!40000 ALTER TABLE `phpbt_auth_perm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_auth_user`
--

DROP TABLE IF EXISTS `phpbt_auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_auth_user` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `login` char(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `first_name` char(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `last_name` char(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `email` char(60) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `password` char(60) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `bug_list_fields` text COLLATE utf8mb4_spanish_ci,
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_auth_user`
--

LOCK TABLES `phpbt_auth_user` WRITE;
/*!40000 ALTER TABLE `phpbt_auth_user` DISABLE KEYS */;
INSERT INTO `phpbt_auth_user` VALUES (0,'Anonymous User','Anonymous','User','','',0,NULL,0,0,0,0),(1,'diego.sanchez@istea.com.ar','System','Admin','diego.sanchez@istea.com.ar','63a9f0ea7bb98050796b649e85481845',1,NULL,0,0,0,0);
/*!40000 ALTER TABLE `phpbt_auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_auth_user_seq`
--

DROP TABLE IF EXISTS `phpbt_auth_user_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_auth_user_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_auth_user_seq`
--

LOCK TABLES `phpbt_auth_user_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_auth_user_seq` DISABLE KEYS */;
INSERT INTO `phpbt_auth_user_seq` VALUES (1);
/*!40000 ALTER TABLE `phpbt_auth_user_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bookmark`
--

DROP TABLE IF EXISTS `phpbt_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bookmark` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `bug_id_bookmark` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bookmark`
--

LOCK TABLES `phpbt_bookmark` WRITE;
/*!40000 ALTER TABLE `phpbt_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug`
--

DROP TABLE IF EXISTS `phpbt_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug` (
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `severity_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `priority` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `status_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `resolution_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `database_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `site_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `assigned_to` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `version_id` int(10) unsigned NOT NULL DEFAULT '0',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0',
  `os_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `browser_string` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `close_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `closed_in_version_id` int(10) unsigned NOT NULL DEFAULT '0',
  `to_be_closed_in_version_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug`
--

LOCK TABLES `phpbt_bug` WRITE;
/*!40000 ALTER TABLE `phpbt_bug` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug_cc`
--

DROP TABLE IF EXISTS `phpbt_bug_cc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug_cc` (
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bug_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug_cc`
--

LOCK TABLES `phpbt_bug_cc` WRITE;
/*!40000 ALTER TABLE `phpbt_bug_cc` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug_cc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug_dependency`
--

DROP TABLE IF EXISTS `phpbt_bug_dependency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug_dependency` (
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `depends_on` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bug_id`,`depends_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug_dependency`
--

LOCK TABLES `phpbt_bug_dependency` WRITE;
/*!40000 ALTER TABLE `phpbt_bug_dependency` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug_dependency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug_group`
--

DROP TABLE IF EXISTS `phpbt_bug_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug_group` (
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bug_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug_group`
--

LOCK TABLES `phpbt_bug_group` WRITE;
/*!40000 ALTER TABLE `phpbt_bug_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug_history`
--

DROP TABLE IF EXISTS `phpbt_bug_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug_history` (
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `changed_field` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `old_value` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `new_value` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug_history`
--

LOCK TABLES `phpbt_bug_history` WRITE;
/*!40000 ALTER TABLE `phpbt_bug_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug_seq`
--

DROP TABLE IF EXISTS `phpbt_bug_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug_seq`
--

LOCK TABLES `phpbt_bug_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_bug_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_bug_vote`
--

DROP TABLE IF EXISTS `phpbt_bug_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_bug_vote` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`bug_id`),
  KEY `bug_id` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_bug_vote`
--

LOCK TABLES `phpbt_bug_vote` WRITE;
/*!40000 ALTER TABLE `phpbt_bug_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_bug_vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_comment`
--

DROP TABLE IF EXISTS `phpbt_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_comment` (
  `comment_id` int(10) unsigned NOT NULL DEFAULT '0',
  `bug_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_text` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `bug_id_comment` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_comment`
--

LOCK TABLES `phpbt_comment` WRITE;
/*!40000 ALTER TABLE `phpbt_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_comment_seq`
--

DROP TABLE IF EXISTS `phpbt_comment_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_comment_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_comment_seq`
--

LOCK TABLES `phpbt_comment_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_comment_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_comment_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_component`
--

DROP TABLE IF EXISTS `phpbt_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_component` (
  `component_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `component_name` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `component_desc` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `owner` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_component`
--

LOCK TABLES `phpbt_component` WRITE;
/*!40000 ALTER TABLE `phpbt_component` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_component_seq`
--

DROP TABLE IF EXISTS `phpbt_component_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_component_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_component_seq`
--

LOCK TABLES `phpbt_component_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_component_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_component_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_configuration`
--

DROP TABLE IF EXISTS `phpbt_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_configuration` (
  `varname` char(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `varvalue` char(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `description` char(255) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `vartype` char(20) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`varname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_configuration`
--

LOCK TABLES `phpbt_configuration` WRITE;
/*!40000 ALTER TABLE `phpbt_configuration` DISABLE KEYS */;
INSERT INTO `phpbt_configuration` VALUES ('ADMIN_EMAIL','phpbt@dhsbt.com','The email address used in correspondence from the bug tracker','string'),('ATTACHMENT_MAX_SIZE','2097152','Maximum size (in bytes) of an attachment. This will not override the settings in php.ini if php.ini has a lower limit.','string'),('ATTACHMENT_PATH','attachments','Sub-dir of the INSTALLPATH - Needs to be writeable by the web process','string'),('BUG_ASSIGNED','3','The status to assign a bug when it is assigned.','multi'),('BUG_PROMOTED','2','The status to assign a bug when it is promoted (if enabled).','multi'),('BUG_REOPENED','6','The status to assign a bug when it is reopened.','multi'),('BUG_UNCONFIRMED','1','The status to assign a bug when it is first submitted.','multi'),('CVS_WEB','http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/phpbt/phpbt/','Location of your cvs web interface (see format_comments() in bug.php)','string'),('DATE_FORMAT','Y-m-d','See the <a href=\"http://www.php.net/date\" target=\"_new\">date page</a> in the PHP manual for more info','string'),('DB_VERSION','21','Database Version <b>Warning:</b> Changing this might make things go horribly wrong, so do not change it.','mixed'),('EMAIL_DISABLED','0','Whether to disable all mail sent from the system','bool'),('EMAIL_IS_LOGIN','1','Whether to use email addresses as logins','bool'),('ENCRYPT_PASS','1','Whether to store passwords encrypted.  <b>Warning:</b> Changing this after users have been created will result in their being unable to login.','bool'),('FORCE_LOGIN','0','Force users to login before being able to use the bug tracker','bool'),('HIDE_EMAIL','1','Should email addresses be hidden for those not logged in?','bool'),('INSTALL_URL','http://dhsbt.com/','The base URL of the phpBugTracker installation','string'),('JPGRAPH_PATH','','If not in the include path.  This is the file path on the web server, not a URL.','string'),('LANGUAGE','en','The language file to use for warning and error messages','multi'),('MASK_EMAIL','1','Should email addresses have . changed to \'dot\' and @ change to \'at\'?','bool'),('MAX_USER_VOTES','5','The maximum number of votes a user can cast across all bugs (Set to 0 to have no limit)','string'),('NEW_ACCOUNTS_DISABLED','0','Only admins can create new user accounts - newaccount.php is disabled','bool'),('NEW_ACCOUNTS_GROUP','User','The group assigned to new user accounts','string'),('PROMOTE_VOTES','5','The number of votes required to promote a bug from Unconfirmed to New (Set to 0 to disable promotions by voting)','string'),('RECALL_LOGIN','0','Enable use of cookies to store username between logins','bool'),('SEND_MIME_EMAIL','1','Whether to use MIME quoted-printable encoded emails or not','bool'),('SHOW_PROJECT_SUMMARIES','1','Itemize bug stats by project on the home page','bool'),('STRICT_UPDATING','0','Only the bug reporter, bug owner, managers, and admins can change a bug','bool'),('STYLE','default','The CSS file to use (color scheme)','multi'),('THEME','default','Which set of templates to use','multi'),('TIME_FORMAT','g:i A','See the <a href=\"http://www.php.net/date\" target=\"_new\">date page</a> in the PHP manual for more info','string'),('USE_JPGRAPH','','Whether to show some reports as images','bool'),('USE_PRIORITY_COLOR','1','Should the query list use the priority colors as the field background color','bool'),('USE_PRIORITY_COLOR_LINE','0','Should the query list use the priority colors as the row background color','bool'),('USE_SEVERITY_COLOR','1','Should the query list use the severity colors as the field background color','bool'),('USE_SEVERITY_COLOR_LINE','0','Should the query list use the severity colors as the row background color (like SourceForge)','bool');
/*!40000 ALTER TABLE `phpbt_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_database_server`
--

DROP TABLE IF EXISTS `phpbt_database_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_database_server` (
  `database_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `database_name` varchar(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`database_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_database_server`
--

LOCK TABLES `phpbt_database_server` WRITE;
/*!40000 ALTER TABLE `phpbt_database_server` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_database_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_database_server_seq`
--

DROP TABLE IF EXISTS `phpbt_database_server_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_database_server_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_database_server_seq`
--

LOCK TABLES `phpbt_database_server_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_database_server_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_database_server_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_group_perm`
--

DROP TABLE IF EXISTS `phpbt_group_perm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_group_perm` (
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `perm_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`perm_id`),
  KEY `perm_id` (`perm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_group_perm`
--

LOCK TABLES `phpbt_group_perm` WRITE;
/*!40000 ALTER TABLE `phpbt_group_perm` DISABLE KEYS */;
INSERT INTO `phpbt_group_perm` VALUES (1,1),(3,2),(3,3),(4,3),(3,4),(3,5),(7,5),(3,6),(4,6),(9,6),(3,7),(4,7),(7,7),(8,7),(9,7),(3,8),(8,8),(9,8),(3,9),(8,9),(9,9),(3,10),(9,10),(3,11),(8,11),(9,11),(3,12),(4,12),(3,13),(4,13),(9,13),(3,14),(4,14);
/*!40000 ALTER TABLE `phpbt_group_perm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_os`
--

DROP TABLE IF EXISTS `phpbt_os`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_os` (
  `os_id` int(10) unsigned NOT NULL DEFAULT '0',
  `os_name` char(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `regex` char(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`os_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_os`
--

LOCK TABLES `phpbt_os` WRITE;
/*!40000 ALTER TABLE `phpbt_os` DISABLE KEYS */;
INSERT INTO `phpbt_os` VALUES (1,'N/A',1,'');
/*!40000 ALTER TABLE `phpbt_os` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_os_seq`
--

DROP TABLE IF EXISTS `phpbt_os_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_os_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_os_seq`
--

LOCK TABLES `phpbt_os_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_os_seq` DISABLE KEYS */;
INSERT INTO `phpbt_os_seq` VALUES (31);
/*!40000 ALTER TABLE `phpbt_os_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_priority`
--

DROP TABLE IF EXISTS `phpbt_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_priority` (
  `priority_id` int(10) unsigned NOT NULL DEFAULT '0',
  `priority_name` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `priority_desc` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `priority_color` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '#FFFFFF',
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_priority`
--

LOCK TABLES `phpbt_priority` WRITE;
/*!40000 ALTER TABLE `phpbt_priority` DISABLE KEYS */;
INSERT INTO `phpbt_priority` VALUES (1,'Low','Fix if possible',1,'#dadada'),(2,'Medium Low','Must fix before final',2,'#dad0d0'),(3,'Medium','Fix before next milestone (alpha, beta, etc.)',3,'#dac0c0'),(4,'Medium High','Fix as soon as possible',4,'#dab0b0'),(5,'High','Fix immediately',5,'#ff9999');
/*!40000 ALTER TABLE `phpbt_priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_priority_seq`
--

DROP TABLE IF EXISTS `phpbt_priority_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_priority_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_priority_seq`
--

LOCK TABLES `phpbt_priority_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_priority_seq` DISABLE KEYS */;
INSERT INTO `phpbt_priority_seq` VALUES (5);
/*!40000 ALTER TABLE `phpbt_priority_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_project`
--

DROP TABLE IF EXISTS `phpbt_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_project` (
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_name` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `project_desc` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_project`
--

LOCK TABLES `phpbt_project` WRITE;
/*!40000 ALTER TABLE `phpbt_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_project_group`
--

DROP TABLE IF EXISTS `phpbt_project_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_project_group` (
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_project_group`
--

LOCK TABLES `phpbt_project_group` WRITE;
/*!40000 ALTER TABLE `phpbt_project_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_project_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_project_perm`
--

DROP TABLE IF EXISTS `phpbt_project_perm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_project_perm` (
  `project_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_project_perm`
--

LOCK TABLES `phpbt_project_perm` WRITE;
/*!40000 ALTER TABLE `phpbt_project_perm` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_project_perm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_project_seq`
--

DROP TABLE IF EXISTS `phpbt_project_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_project_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_project_seq`
--

LOCK TABLES `phpbt_project_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_project_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_project_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_resolution`
--

DROP TABLE IF EXISTS `phpbt_resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_resolution` (
  `resolution_id` int(10) unsigned NOT NULL DEFAULT '0',
  `resolution_name` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `resolution_desc` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`resolution_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_resolution`
--

LOCK TABLES `phpbt_resolution` WRITE;
/*!40000 ALTER TABLE `phpbt_resolution` DISABLE KEYS */;
INSERT INTO `phpbt_resolution` VALUES (1,'Fixed','Bug was eliminated',1),(2,'Not a bug','It\'s not a bug -- it\'s a feature!',2),(3,'Won\'t Fix','This bug will stay',3),(4,'Deferred','We\'ll get around to it later',4),(5,'Works for me','Can\'t replicate the bug',5),(6,'Duplicate','',6);
/*!40000 ALTER TABLE `phpbt_resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_resolution_seq`
--

DROP TABLE IF EXISTS `phpbt_resolution_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_resolution_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_resolution_seq`
--

LOCK TABLES `phpbt_resolution_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_resolution_seq` DISABLE KEYS */;
INSERT INTO `phpbt_resolution_seq` VALUES (6);
/*!40000 ALTER TABLE `phpbt_resolution_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_saved_query`
--

DROP TABLE IF EXISTS `phpbt_saved_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_saved_query` (
  `saved_query_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `saved_query_name` varchar(40) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `saved_query_string` text COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`saved_query_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_saved_query`
--

LOCK TABLES `phpbt_saved_query` WRITE;
/*!40000 ALTER TABLE `phpbt_saved_query` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_saved_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_severity`
--

DROP TABLE IF EXISTS `phpbt_severity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_severity` (
  `severity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `severity_name` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `severity_desc` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `severity_color` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '#FFFFFF',
  PRIMARY KEY (`severity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_severity`
--

LOCK TABLES `phpbt_severity` WRITE;
/*!40000 ALTER TABLE `phpbt_severity` DISABLE KEYS */;
INSERT INTO `phpbt_severity` VALUES (1,'Unassigned','Default bug creation',1,'#dadada'),(2,'Idea','Ideas for further development',2,'#dad0d0'),(3,'Feature Request','Requests for specific features',3,'#dacaca'),(4,'Annoyance','Cosmetic problems or bugs not affecting performance',4,'#dac0c0'),(5,'Content','Non-functional related bugs, such as text content',5,'#dababa'),(6,'Significant','A bug affecting the intended performance of the product',6,'#dab0b0'),(7,'Critical','A bug severe enough to prevent the release of the product',7,'#ff9999');
/*!40000 ALTER TABLE `phpbt_severity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_severity_seq`
--

DROP TABLE IF EXISTS `phpbt_severity_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_severity_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_severity_seq`
--

LOCK TABLES `phpbt_severity_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_severity_seq` DISABLE KEYS */;
INSERT INTO `phpbt_severity_seq` VALUES (7);
/*!40000 ALTER TABLE `phpbt_severity_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_site`
--

DROP TABLE IF EXISTS `phpbt_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_site` (
  `site_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `site_name` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_site`
--

LOCK TABLES `phpbt_site` WRITE;
/*!40000 ALTER TABLE `phpbt_site` DISABLE KEYS */;
INSERT INTO `phpbt_site` VALUES (0,'N/A',1);
/*!40000 ALTER TABLE `phpbt_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_site_seq`
--

DROP TABLE IF EXISTS `phpbt_site_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_site_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_site_seq`
--

LOCK TABLES `phpbt_site_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_site_seq` DISABLE KEYS */;
INSERT INTO `phpbt_site_seq` VALUES (4);
/*!40000 ALTER TABLE `phpbt_site_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_status`
--

DROP TABLE IF EXISTS `phpbt_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_status` (
  `status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status_name` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `status_desc` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `bug_open` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_status`
--

LOCK TABLES `phpbt_status` WRITE;
/*!40000 ALTER TABLE `phpbt_status` DISABLE KEYS */;
INSERT INTO `phpbt_status` VALUES (1,'New Report','Newly submitted report.',1,1),(2,'Reviewed','Module owner has looked at report, properly categorized it.',2,1),(3,'Assigned','Assigned to a developer.',3,1),(4,'Needs QA','Set by engineer with a resolution, needs to be verified.',4,1),(5,'Verified','The resolution is confirmed by the reporter',5,1),(6,'Reopened','Closed but opened again for further inspection',6,1),(7,'Closed','The bug is officially squashed.',7,0);
/*!40000 ALTER TABLE `phpbt_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_status_seq`
--

DROP TABLE IF EXISTS `phpbt_status_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_status_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_status_seq`
--

LOCK TABLES `phpbt_status_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_status_seq` DISABLE KEYS */;
INSERT INTO `phpbt_status_seq` VALUES (7);
/*!40000 ALTER TABLE `phpbt_status_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_user_group`
--

DROP TABLE IF EXISTS `phpbt_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_user_group` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_user_group`
--

LOCK TABLES `phpbt_user_group` WRITE;
/*!40000 ALTER TABLE `phpbt_user_group` DISABLE KEYS */;
INSERT INTO `phpbt_user_group` VALUES (1,1,0,0);
/*!40000 ALTER TABLE `phpbt_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_user_perm`
--

DROP TABLE IF EXISTS `phpbt_user_perm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_user_perm` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `perm_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`perm_id`),
  KEY `perm_id` (`perm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_user_perm`
--

LOCK TABLES `phpbt_user_perm` WRITE;
/*!40000 ALTER TABLE `phpbt_user_perm` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_user_perm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_user_pref`
--

DROP TABLE IF EXISTS `phpbt_user_pref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_user_pref` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email_notices` tinyint(1) NOT NULL DEFAULT '1',
  `saved_queries` tinyint(1) NOT NULL DEFAULT '1',
  `def_results` int(11) NOT NULL DEFAULT '20',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_user_pref`
--

LOCK TABLES `phpbt_user_pref` WRITE;
/*!40000 ALTER TABLE `phpbt_user_pref` DISABLE KEYS */;
INSERT INTO `phpbt_user_pref` VALUES (0,1,1,20),(1,1,1,20);
/*!40000 ALTER TABLE `phpbt_user_pref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_version`
--

DROP TABLE IF EXISTS `phpbt_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_version` (
  `version_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `version_name` char(30) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `last_modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `last_modified_date` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_version`
--

LOCK TABLES `phpbt_version` WRITE;
/*!40000 ALTER TABLE `phpbt_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbt_version_seq`
--

DROP TABLE IF EXISTS `phpbt_version_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbt_version_seq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbt_version_seq`
--

LOCK TABLES `phpbt_version_seq` WRITE;
/*!40000 ALTER TABLE `phpbt_version_seq` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbt_version_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dhsbt'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-01 14:25:06
